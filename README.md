# Once Upon a Time

C'est l'histoire d'une petite grenouille

Cette grenouille aime aller à la plage

Mais il fait très chaud

Elle a besoin d'un parasol

## Chapitre 01 - le magasin

La petite grenouille décida d'aller
au magasin pour acheter un parasol

## Chapitre 02 - le restaurant

La petite grenouille est très gourmande

## Chapitre 03 - la maison

La petite grenouille habite au bord d'un bel étang

## Chapitre 04 - les vacances

Pour les vacances, la petite grenouille aime bien
aller voir ses frères et soeurs dans le sud de la forêt.
Ce n'est pas très loin, mais ça lui prend une demi journée.

C'est très long même pour une distance courte car le bus
fait le tour complet de la forêt avec beaucoups d'arrêts.
